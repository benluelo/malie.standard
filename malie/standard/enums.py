from enum import StrEnum
from typing import cast


class UpperStrEnum(StrEnum):
    @staticmethod
    def _generate_next_value_(name: str, start: int, count: int, last_values) -> str:
        return name


# Let's define some enums! For fun and profit.

CardType = UpperStrEnum(
    "CardType",
    [
        "POKEMON",
        "TRAINER",
        "ENERGY",
    ],
)

CardSubType = UpperStrEnum(
    "CardSubType",
    [
        "ITEM",
        "SUPPORTER",
        "STADIUM",
        "TOOL",
        "BASIC",
        "SPECIAL",
    ],
)

CardSize = UpperStrEnum(
    "CardSize",
    [
        "STANDARD",
    ],
)

CardBack = UpperStrEnum(
    "CardBack",
    [
        "POCKET_MONSTERS_1996",
        "POKEMON_1999",
        "POKEMON_2002",
    ],
)

TextKind = UpperStrEnum(
    "TextKind",
    [
        "ABILITY",
        "ATTACK",
        "EFFECT",
        "REMINDER",
        "RULE_BOX",
        "TEXT_BOX",
    ],
)

RarityDesignation = UpperStrEnum(
    "RarityDesignation",
    [
        "PROMO",
        "COMMON",
        "UNCOMMON",
        "RARE",
        "DOUBLE_RARE",
        "ILLUSTRATION_RARE",
        "ULTRA_RARE",
        "SPECIAL_ILLUSTRATION_RARE",
        "HYPER_RARE",
    ],
)

RarityIcon = UpperStrEnum(
    "RarityIcon",
    [
        "BLACK_STAR_PROMO",
        "BLACK_CIRCLE",
        "BLACK_DIAMOND",
        "BLACK_STAR",
        "TWO_BLACK_STARS",
        "TWO_SILVER_STARS",
        "GOLD_STAR",
        "TWO_GOLD_STARS",
        "THREE_GOLD_STARS",
    ],
)

Tag = UpperStrEnum(
    "Tag",
    [
        "ITEM",
        "TOOL",
        "PLAYABLE_TRAINER",
        "EX_LOWER",
        "TERA",
        "ANCIENT",
        "FUTURE",
    ],
)

Color = UpperStrEnum(
    "Color",
    [
        "GRASS",
        "FIRE",
        "WATER",
        "LIGHTNING",
        "PSYCHIC",
        "FIGHTING",
        "DARKNESS",
        "METAL",
        "FAIRY",
        "DRAGON",
        "COLORLESS",
        "FREE",
    ],
)

FoilType = UpperStrEnum(
    "FoilType",
    [
        "SV_HOLO",
        "SV_ULTRA",
        "FLAT_SILVER",
        "SUN_PILLAR",
        "COSMOS",
        "STAMPED",
    ],
)

FoilMask = UpperStrEnum(
    "FoilMask",
    [
        "HOLO",
        "REVERSE",
        "ETCHED",
        "STAMPED",
    ],
)

PokemonStage = UpperStrEnum(
    "PokemonStage",
    [
        "BASIC",
        "STAGE1",
        "STAGE2",
    ],
)


class AllOperators(StrEnum):
    MULTIPLY = "×"
    MINUS = "-"
    PLUS = "+"


class WeaknessOperator(StrEnum):
    MULTIPLY = str(AllOperators.MULTIPLY)


class ResistanceOperator(StrEnum):
    MINUS = str(AllOperators.MINUS)


class DamageSuffix(StrEnum):
    PLUS = str(AllOperators.PLUS)
    MINUS = str(AllOperators.MINUS)
    MULTIPLY = str(AllOperators.MULTIPLY)
