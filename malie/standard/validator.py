import json
import sys


try:
    import requests
except ModuleNotFoundError:
    requests = None

from .model import ExportDocument


def main():
    filename = sys.argv[1]
    if filename.startswith("http"):
        if requests:
            rsp = requests.get(filename, timeout=15)
            data = rsp.json()
        else:
            raise RuntimeError(f"requests library not found to fetch from '{filename}'")
    else:
        with open(filename, encoding="utf-8") as infile:
            data = json.load(infile)

    print(f"validate '{sys.argv[1]}' ... ", end="")
    ExportDocument.parse_obj(data)
    print("OK")


if __name__ == "__main__":
    main()
