malie.io Pokémon Trading Card Game export format
################################################

This document specifies the format used by malie.io to export Pokémon
Trading Card Game data.

This is version XXXXXXXX of the draft specification, corresponding
with reference exports of TCGL data in
https://cdn.malie.io/file/malie-io/tcgl/export/index.json.

.. |G| image:: _static/svg/Grass.svg
       :alt: Grass energy symbol
       :class: svg_icon

.. |R| image:: _static/svg/Fire.svg
       :alt: Fire energy symbol
       :class: svg_icon

.. |W| image:: _static/svg/Water.svg
       :alt: Water energy symbol
       :class: svg_icon

.. |L| image:: _static/svg/Lightning.svg
       :alt: Lightning energy symbol
       :class: svg_icon

.. |P| image:: _static/svg/Psychic.svg
       :alt: Psychic energy symbol
       :class: svg_icon

.. |F| image:: _static/svg/Fighting.svg
       :alt: Fighting energy symbol
       :class: svg_icon

.. |D| image:: _static/svg/Darkness.svg
       :alt: Darkness energy symbol
       :class: svg_icon

.. |M| image:: _static/svg/Metal.svg
       :alt: Metal energy symbol
       :class: svg_icon

.. |Y| image:: _static/svg/Fairy.svg
       :alt: Fairy energy symbol
       :class: svg_icon

.. |N| image:: _static/svg/Dragon.svg
       :alt: Dragon energy symbol
       :class: svg_icon

.. |C| image:: _static/svg/Colorless.svg
       :alt: Colorless energy symbol
       :class: svg_icon

.. |CO| image:: _static/svg/common.svg
	:alt: Common rarity symbol
	:class: svg_icon

.. |UN| image:: _static/svg/uncommon.svg
	:alt: Uncommon rarity symbol
	:class: svg_icon

.. |RA| image:: _static/svg/rare.svg
	:alt: Rare rarity symbol
	:class: svg_icon

.. |DR| image:: _static/svg/double_rare.svg
	:alt: Double Rare rarity symbol
	:class: svg_icon

.. |IR| image:: _static/svg/illustration_rare.svg
	:alt: Illustration Rare rarity symbol
	:class: svg_icon

.. |UR| image:: _static/svg/ultra_rare.svg
	:alt: Ultra Rare rarity symbol
	:class: svg_icon

.. |SIR| image:: _static/svg/special_illustration_rare.svg
	:alt: Special Illustration Rare rarity symbol
	:class: svg_icon

.. |HR| image:: _static/svg/hyper_rare.svg
	:alt: Hyper Rare rarity symbol
	:class: svg_icon

.. |BSP| image:: _static/svg/black_star_promo.svg
         :alt: Black Star Promo symbol
	 :class: svg_icon


Authors
=======

* nago <nago@malie.io> - malie.io & pkmncards.com
* Ben Luelo <ben@luelo.dev> - tcgcollector.com

.. note::

   This specification is a draft. If you would like to receive
   authorship credit for your feedback, suggestions, or review, please
   let nago know how you would like to be credited and they will
   happily add you.

   Conversely, a name appearing on this list does not necessarily infer
   endorsement of this draft.


Copyright and Licensing
=======================

.. admonition:: FIXME

   "© nago 2023" for now, need to figure this out

The authors of this document are not affiliated in any way, shape or
form with The Pokémon Company International, The Pokémon Company ("株
式会社ポケモン", "Kabushiki Gaisha Pokémon"), nor any subsidiaries
thereof, past or present.

Introduction
============

This specification document defines the common format used by malie.io
to export Pokémon Trading Card Game data and images.

Goals
-----

This format can't reasonably be everything to everyone. The goals of
this specification are:


It should be a good interchange format.

  It should serve as a reasonable basis for writing tooling without
  needing to worry about the specifics of a particular data source's
  quirks.

  This format should provide a reasonable basis for normalizing data
  extracted both from official and unofficial sources, such as tcgo,
  tcgl, carddex, pokemon.com, pkmncards.com, etc.

It should be easy to understand as plaintext by a human:

  The data should both make sense as a machine-readable export and as
  something a human can read and make sense of. This cuts down on the
  number of formats we need to define and creates a more obvious
  "single source of truth" for the definition of a card.

It should be easy to edit by a human:

  Not all cards that exist are defined in digital sources; some cards
  will need to be defined by hand. In service of the "single source of
  truth" goal, the ability to submit plaintext corrections by
  casual contributors is vital to the accuracy and reliability
  of the data.

  Importantly, even official digital sources are incorrect. The
  ability to diff, edit, and submit changes easily is a crucial
  feature for the format.

  Natural language strings are preferred whenever possible instead of
  enumerated numerical IDs. You should be able to edit the file as-is
  without needing to "translate" to or from a machine-centric format
  whenever possible.

The format should be language agnostic.

  Most of the data processed by malie.io is not in English;
  localization must be considered and integrated from the outset of
  the format.

  (Ideally, the format would not lean on English quite as heavily as
  it does for enumeration names and definitions. Cards are usually
  defined in Japanese first, but Japanese cards are currently out of
  scope and, in service of having any specification at all, we
  begrudgingly accept English as the lingua franca for enumerations.

  At the end of the day, English is what I speak and I'm doing the
  best I can!)

Prefer a specification that minimizes coordination:

  Ideally, when new cards are released, it should be unambiguous for
  how to represent them in the data format without requiring a central
  authority to issue reference output or provide guidance or issue
  hotfixes to the specification.

  The longevitiy of a standard here is perceived to be linked to its
  usefulness for real sites to operate, and real sites often cannot
  wait for a standards body to provide updates to a specification on
  release days.

Represent information as it exists *on the card*:

  By deferring to the information printed on the card wherever
  possible, the canonical source of truth is unambiguous and does not
  rely on cooperation between disparate parties.

Avoid data that is prone to change over time:

  The definition for a card should be "set in stone" when it is
  created and should not function as a living document. Fields like
  "evolves into" are not feasible as part of this specification and
  should be maintained elsewhere.

  This also precludes external links and information as a rule;
  e.g. links to Bulbapedia, Troll & Toad, TCGPlayer and so on are too
  volatile for inclusion here.

Avoid data that is external to the card itself:

  In general, prefer describing the card itself from what can be
  observed on the card and avoid relying on external sources.

  This cannot always be helped - for example, cards prior to Scarlet &
  Violet do not have any text that describes what expansion they are
  from, but this information is considered crucial to the functioning
  of an interchange format.

  Essential information like card rarity names are not defined on the
  card itself, either. In these cases we settle for secondary primary
  sources such as the Elite Trainer Box's player's guide and
  pokemon.com.

Bend the rules when it's really helpful anyway:

  We acknowledge that the goals as laid out above are not always
  strictly possible and that there are valid usability concerns that
  necessitate bending the rules. Oh well!


Non-Goals
---------

* This is a specification for an interchange format, i.e., the export
  and transfer of data about Pokémon cards. It is not a goal of this
  specification to provide any considerations about how this data
  would be searched by a non-technical end user, nor to give any
  consideration to relational database schema or the normalization
  thereof. These concerns are considered to be implementation details
  and not relevant to the interchange format itself.

* Filesize or bandwidth considerations are not considered. JSON is
  notoriously wasteful of bandwidth due to its repeated plaintext
  keys, making this a terrible binary format for a production
  website. The mechanics of translating this specification for an
  efficient streaming protocol such as protobuf is beyond the
  scope of this document.

* This format does not make any attempt to consider or define
  efficient in-memory representations of card objects; only the JSON
  export format is described here.

* This specification does not cover how to handle errata. If the card
  has an error on it such as the wrong artist, the wrong energy cost,
  the wrong Pokémon name, etc - this specification does not cover how
  to represent that information. This specification covers solely "The
  card exactly as printed".

  Errata will be handled separately and elsewhere.


Scope
-----

The current version of this specification covers Scarlet & Violet
expansions from Pokémon TCG Live only.

*Future revisions* of this specification *will* seek to cover all
Wizards of the Coast / Pokémon USA / The Pokémon Company International
published cards in all languages. Even later revisions may seek to add APAC
region cards. The current scope is being limited for two reasons:

(1) We need an operational specification that addresses the immediate
    need of exporting data from Pokémon Trading Card Game Live to
    update fan sites, databases, etc - and this only involves Scarlet
    & Violet for TPCi cards in that immediate use case, and

(2) Reducing the scope allows us to focus on a smaller set of cards
    and review the specification more thoroughly. This will allow us
    to incrementally broaden the scope over time and in future
    revisions.

Wherever reasonable, cards from outside of this scope are considered
to avoid needing to rework the fundamental schema of the
specification. For example, there are cards which feature multiple
types and multiple illustrators, so the schema presented here
accommodates those features even though they do not occur on any cards
within the stated scope.

Wherever the schema may be amended by the simple addition of new
enumerations to handle older (or newer) cards, these amendments are
considered out-of-scope and will be handled at a later date. (I.e.,
new rarity names, new Pokémon stages, etc.)

The current scope for exhaustive review excludes cards from outside of
the TPCi publishing region, notably the APAC regions (Japanese,
Korean, Thai, Indonesian, Chinese, etc.)


Overview
========

The export format is a JSON document consisting of a list of card
objects. Each card object represents a single card in a single
language; different variants of each card (different foiling, etc)
will each have their own object instance.

The encoding of this JSON document must be UTF-8.

The name of each field below is followed by the type of that field in
parentheses. When the type is "string", the type will be clarified as
either "i18n str" when the contents of this field are likely to change
based on the language of the card, or "enum str" when the contents of
this field do **NOT** vary with the language of the card.

Not all fields marked as "i18n str" will actually vary between
languages; but this marker denotes that the field is to represent the
text as it appears on the card and it MAY vary between languages
should the circumstance ever arise.

All fields will be marked as Optional ("nullable") or Required (must
always be present). The definition of "Optional" here does not mean
the field value can be omitted at one's discretion, but rather that
the field is not guaranteed to hold a value for all cards within
scope.

A field with a False-y value (empty string, 0, empty array, empty
object, &c), an explicitly nulled field, and an *absent* field are all
semantically equivalent. Not all fields that may take a "False-y"
value permit omission or null values, however.

Common Fields
=============

All card objects generally fall into two categories: *Pokémon* and
*Everything Else*, though this taxonomy is not formally defined and is
not strictly adhered to.

The fields described in this section are common to all card types,
although some fields are Optional and may not be defined for a given
card's record.

The class hierarchy this suggests is not perfectly stringent; some
fields that are "Optional" are in practice always defined except for
certain circumstances that do not fall neatly along class boundaries:

 * ``subtype`` is not currently used for ``POKEMON`` cards.
 * ``artists`` does not happen to be defined for any ``ENERGY`` cards
   in scope, but it *would* be if any Energy cards did list an
   illustrator. Classic WOTC cards *did* list illustrator credits for
   Energy.
 * ``regulation_mark`` also does not happen to be defined for any
   ``ENERGY`` - ``BASIC`` cards, but it *would* be if any Basic Energy
   cards did happen to feature a regulation mark.
 * Some ``tags`` are only semantically valid for certain categories of
   cards. There are no tags currently defined that apply to any Energy
   cards in-scope, but there's no fundamental reason why this will or
   should remain true.

On rare occasion, there are cards that strain the boundaries of the
``card_type`` classification:

 * Cards with the ``PLAYABLE_TRAINER`` tag will also have the ``hp``
   field set, as if they were a Pokémon. The ``hp`` field is required
   for Pokémon, but Optional for Trainers.
 * The ``types`` field is required for ``POKEMON``, but is also
   required for ``ENERGY`` - ``BASIC`` cards too!

``lang`` (enum str, Required)
-----------------------------

The language as written and used on the card, as a `BCP 47 language
tag <https://en.wikipedia.org/wiki/IETF_language_tag>`_.

This field does not describe the region in which a card was published;
it describes the language present on the face of the card.

Under the current scope of this specification (TCGL cards only,
SV-on), this **should** be one of the following languages:

* ``de-DE`` (🇩🇪 Deutsch)
* ``en-US`` (🇺🇸 American English)
* ``es-ES`` (🇪🇸 Español)
* ``fr-FR`` (🇫🇷 Français)
* ``it-IT`` (🇮🇹 Italiano)
* ``pt-BR`` (🇧🇷 português brasileiro)

All cards published and distributed by TPCi since Scarlet & Violet
released are one of six languages: English, German, Spanish, French,
Italian or Brazilian Portuguese.

Additional languages are presently beyond the scope of this
specification; including all of the APAC regions and languages
(Japanese, Korean, Indonesian, Thai and Chinese). Other langauges that
are not in current production are also beyond the scope of this
document (Russian, Polish, Dutch, Portuguese).

All official Portuguese cards produced in this timeframe have been for
the LATAM/Brazillian market specifically, and so "pt-BR" is preferred
to "pt-PT". "pt-PT" cards have been produced in the past, but the
specifics of enumerating which ones is currently beyond the scope of
this document.

The majority of English cards produced in this timeframe have been for
the North American market, and so "en-US" is preferred. e.g.,
"Grafaiai" has an attack named "Colorful Graffiti" and not "Colourful
Graffiti".

.. admonition:: Out of scope

   Cards not present in TCGL are currently beyond the scope of this
   document, and mercifully, precludes us from discussing the FUTSAL
   promotional cards, which may rightly be "en-GB". 🇬🇧☕💂)


``card_type`` (enum str, Required)
----------------------------------

The gameplay type of this card. One of:

* ``POKEMON``
* ``ENERGY``
* ``TRAINER``

``POKEMON``
^^^^^^^^^^^

``POKEMON`` cards feature a `stage` in the upper-left corner of the
card, `HP` and `types` in the upper-right corner of the card, list
`weakness`, `resistance` and `retreat` information, and have one or
more abilities/attacks in the card text area.

.. card_figure:: pokemon_sel.png
   :exp: sv2
   :lang: en
   :num: 062
   :region: pokemon_header_boxes
   :alt: Pokémon card

``ENERGY``
^^^^^^^^^^

``ENERGY`` cards have the word "Energy" (or the localized equivalent)
in the upper-right corner of the card:

.. card_figure:: energy_sel_en.png
   :exp: sv2
   :lang: en
   :num: 191
   :region: card_type_box
   :alt: Energy card

.. card_figure:: energy_sel_de.png
   :exp: sve
   :lang: de
   :num: 002
   :region: card_type_box
   :alt: Energy card

.. card_figure:: energy_sel_es.png
   :exp: sv1
   :lang: es
   :num: 257
   :region: card_type_box
   :alt: Energy card

.. card_figure:: energy_sel_fr.png
   :exp: sv2
   :lang: fr
   :num: 190
   :region: card_type_box
   :alt: Energy card

.. card_figure:: energy_sel_it.png
   :exp: sv3-5
   :lang: it
   :num: 207
   :region: card_type_box
   :alt: Energy card

``TRAINER``
^^^^^^^^^^^

``TRAINER`` cards have the word "TRAINER" (or the localized
equivalent) in the upper-right corner of the card:

.. card_figure:: trainer_sel_en.png
   :exp: sv1
   :lang: en
   :num: 183
   :region: card_type_box
   :alt: Trainer card (English)

.. card_figure:: trainer_sel_es.png
   :exp: sv1
   :lang: es
   :num: 178
   :region: card_type_box
   :alt: Trainer card (Español)

.. card_figure:: trainer_sel_fr.png
   :exp: sv1
   :lang: fr
   :num: 256
   :region: card_type_box
   :alt: Trainer card (Français)

.. card_figure:: trainer_sel_it.png
   :exp: sv3-5
   :lang: it
   :num: 197
   :region: card_type_box
   :alt: Trainer card (Italiano)

.. card_figure:: trainer_sel_ptbr.png
   :exp: sv3
   :lang: ptbr
   :num: 191
   :region: card_type_box
   :alt: Trainer card (português brasileiro)

.. admonition:: Out of scope

   How to classify Diamond & Pearl era Stadium/Supporter cards, LEGEND
   cards, and V-UNION cards are beyond the current scope of this
   specification and will be addressed in a future revision.

See `Appendix A` for localizations.


``subtype`` (enum str, Optional)
--------------------------------

For ``TRAINER`` and ``ENERGY`` cards, a required additional
classification of the gameplay type of the card. ``POKEMON`` cards do
not utilize this field.

For ``TRAINER`` cards, the subtype is specified on the card in the
upper left, on the same line as "TRAINER" (or the localized
equivalent) in the upper-right. It is one of:

* ``ITEM``
* ``SUPPORTER``
* ``STADIUM``
* ``TOOL``

For ``ENERGY`` cards, the subtype is specified on the card in the
upper left, on the same line as "ENERGY" (or the localized equivalent)
in the upper-right. It is one of:

* ``BASIC``
* ``SPECIAL``


``ITEM``
^^^^^^^^

``ITEM`` cards say "Item" (or the localized equivalent) in the
upper-left hand of the card in blue text, on the same line as
"TRAINER" (or *its* localized equivalent).

.. card_figure:: item_sel_en.png
   :exp: sv1
   :lang: en
   :num: 184
   :region: subtype_box
   :alt: Item card, English

.. card_figure:: item_sel_fr.png
   :exp: sv2
   :lang: fr
   :num: 181
   :region: subtype_box
   :alt: Item card, French

.. card_figure:: item_sel_es.png
   :exp: sv1
   :lang: es
   :num: 196
   :region: subtype_box
   :alt: Item card, Spanish

.. card_figure:: item_sel_it.png
   :exp: sv3
   :lang: it
   :num: 189
   :region: subtype_box
   :alt: Item card, Italian

``SUPPORTER``
^^^^^^^^^^^^^

``SUPPORTER`` cards say "Supporter" (or the localized equivalent) in
the upper-left hand of the card in red text, on the same line as
"TRAINER" (or *its* localized equivalent).

.. card_figure:: subtype_supporter_sel_en.png
   :exp: sv1
   :lang: en
   :num: 251
   :region: subtype_box
   :alt: Supporter card, English

.. card_figure:: subtype_supporter_sel_de.png
   :exp: sv1
   :lang: de
   :num: 239
   :region: subtype_box
   :alt: Supporter card, German

.. card_figure:: subtype_supporter_sel_it.png
   :exp: sv3-5
   :lang: it
   :num: 203
   :region: subtype_box
   :alt: Supporter card, Italian

.. card_figure:: subtype_supporter_sel_es.png
   :exp: sv1
   :lang: es
   :num: 177
   :region: subtype_box
   :alt: Supporter card, Spanish

.. card_figure:: subtype_supporter_sel_ptbr.png
   :exp: sv2
   :lang: ptbr
   :num: 266
   :region: subtype_box
   :alt: Supporter card, Portuguese

``STADIUM``
^^^^^^^^^^^

``STADIUM`` cards say "Stadium" (or the localized equivalent) in the
upper-left hand of the card in green text, on the same line as
"TRAINER" (or *its* localized equivalent).

.. card_figure:: subtype_stadium_sel_en.png
   :exp: svbsp
   :lang: en
   :num: 045
   :region: subtype_box
   :alt: Stadium card, English

.. card_figure:: subtype_stadium_sel_fr.png
   :exp: sv3
   :lang: fr
   :num: 229
   :region: subtype_box
   :alt: Stadium card, French

.. card_figure:: subtype_stadium_sel_it.png
   :exp: sv3-5
   :lang: it
   :num: 157
   :region: subtype_box
   :alt: Stadium card, Italian

.. card_figure:: subtype_stadium_sel_de.png
   :exp: sv1
   :lang: de
   :num: 167
   :region: subtype_box
   :alt: Stadium card, German

.. card_figure:: subtype_stadium_sel_es.png
   :exp: sv1
   :lang: es
   :num: 178
   :region: subtype_box
   :alt: Stadium card, Spanish

.. card_figure:: subtype_stadium_sel_ptbr.png
   :exp: sv3
   :lang: ptbr
   :num: 192
   :region: subtype_box
   :alt: Stadium card, Portuguese

``TOOL``
^^^^^^^^

``TOOL`` cards say "Pokémon Tool" (or the localized equivalent) in the
upper-left hand of the card in purple text, on the same line as
"TRAINER" (or *its* localized equivalent).

.. card_figure:: subtype_tool_sel_en.png
   :exp: sv2
   :lang: en
   :num: 173
   :region: subtype_box
   :alt: Pokémon Tool card, English

.. card_figure:: subtype_tool_sel_fr.png
   :exp: sv1
   :lang: fr
   :num: 174
   :region: subtype_box
   :alt: Pokémon Tool card, French

.. card_figure:: subtype_tool_sel_it.png
   :exp: sv1
   :lang: it
   :num: 192
   :region: subtype_box
   :alt: Pokémon Tool card, Italian

.. card_figure:: subtype_tool_sel_de.png
   :exp: sv1
   :lang: de
   :num: 169
   :region: subtype_box
   :alt: Pokémon Tool card, German

.. card_figure:: subtype_tool_sel_es.png
   :exp: sv3
   :lang: es
   :num: 197
   :region: subtype_box
   :alt: Pokémon Tool card, Spanish

.. card_figure:: subtype_tool_sel_ptbr.png
   :exp: sv2
   :lang: ptbr
   :num: 176
   :region: subtype_box
   :alt: Pokémon Tool card, Portuguese

``BASIC``
^^^^^^^^^

``BASIC`` cards say "Basic Energy" (or the localized equivalent) in the
upper-left hand of the card in darkened text, on the same line as
"ENERGY" (or *its* localized equivalent).

.. card_figure:: subtype_basic_sel_en.png
   :exp: sv2
   :lang: en
   :num: 192
   :region: subtype_box
   :alt: Basic Energy card, English

.. card_figure:: subtype_basic_sel_fr.png
   :exp: sv2
   :lang: fr
   :num: 191
   :region: subtype_box
   :alt: Basic Energy card, French

.. card_figure:: subtype_basic_sel_de.png
   :exp: sv2
   :lang: de
   :num: 193
   :region: subtype_box
   :alt: Basic Energy card, German

.. card_figure:: subtype_basic_sel_it.png
   :exp: sv2
   :lang: it
   :num: 190
   :region: subtype_box
   :alt: Basic Energy card, Italian

.. card_figure:: subtype_basic_sel_es.png
   :exp: sv2
   :lang: es
   :num: 192
   :region: subtype_box
   :alt: Basic Energy card, Spanish


``SPECIAL``
^^^^^^^^^^^

``BASIC`` cards say "Special Energy" (or the localized equivalent) in
the upper-left hand of the card in darkened text, on the same line as
"ENERGY" (or *its* localized equivalent).

.. card_figure:: subtype_special_sel_en.png
   :exp: sv2
   :lang: en
   :num: 192
   :region: subtype_box
   :alt: Special Energy card, English

.. card_figure:: subtype_special_sel_fr.png
   :exp: sv2
   :lang: fr
   :num: 191
   :region: subtype_box
   :alt: Special Energy card, French

.. card_figure:: subtype_special_sel_de.png
   :exp: sv2
   :lang: de
   :num: 193
   :region: subtype_box
   :alt: Special Energy card, German

.. card_figure:: subtype_special_sel_it.png
   :exp: sv2
   :lang: it
   :num: 190
   :region: subtype_box
   :alt: Special Energy card, Italian

.. card_figure:: subtype_special_sel_es.png
   :exp: sv2
   :lang: es
   :num: 192
   :region: subtype_box
   :alt: Special Energy card, Spanish


``name`` (i18n str, Required)
-----------------------------

The name field contains the name of the card as written (where
possible, see below for exceptions).

.. card_figure:: name_sel_pokemon.png
   :exp: sv2
   :lang: en
   :num: 211
   :region: pokemon_name_box
   :alt: Pokemon Card's name field highlighted

.. card_figure:: name_sel_trainer.png
   :exp: sv2
   :lang: en
   :num: 269
   :region: trainer_name_box
   :alt: Trainer Card's name field highlighted

.. card_figure:: name_sel_energy.png
   :exp: sv2
   :lang: en
   :num: 191
   :region: trainer_name_box
   :alt: Energy Card's name field highlighted

Subtitles
^^^^^^^^^

Some cards feature a subtitle that is not considered to be part of the
name, and should be omitted from this field. See the `subtitle` field
definition for more information.

.. card_figure:: subtitle_sel.png
   :exp: sv1
   :lang: en
   :num: 189
   :region: trainer_subtitle_box
   :alt: Trainer Card's subtitle field highlighted

Type symbols
^^^^^^^^^^^^

Names occasionally contain symbols that cannot be represented by
UTF-8. In those cases, a standardized markup is used. For cards that
include energy symbols in the name, the following markup is defined:

* |G| — ``{G}``
* |R| — ``{R}``
* |W| — ``{W}``
* |L| — ``{L}``
* |P| — ``{P}``
* |F| — ``{F}``
* |D| — ``{D}``
* |M| — ``{M}``
* |Y| — ``{Y}``
* |N| — ``{N}``
* |C| — ``{C}``


.. card_figure:: name_sel_energysymbol.png
   :alt: Card with an Energy symbol in its name
   :exp: sve
   :lang: en
   :num: 003
   :region: trainer_name_box

   For example, the name of this card is ``"Basic {W} Energy"``


The single-letter codes are chosen based on official shorthand
from a variety of sources:

* Wizards of the Coast Oracle text uses ``{G|R|W|L|P|F|C}`` shorthand
  from the earliest days of the original Base Set:
  https://web.archive.org/web/20050127072112/http://www.wizards.com:80/pokemon/rulings/Oracle_BaseSet1.txt

* Wizards of the Coast Oracle text for Neo Genesis adds ``{D|M}``
  shorthand codes for |D| |M|:
  https://web.archive.org/web/20050127072152/http://www.wizards.com/pokemon/rulings/Oracle_NeoGenesis.txt

* Pokémon-USA "Card Dex" PDFs also use ``{G|R|W|L|P|F|D|M|C}``:
  https://assets.pokemon.com/assets/cms/pdf/tcg/carddex/black_white.pdf

* ``{N}``, representing |N| can be seen in the Card Dex PDFs for
  Dragons Exalted:
  https://assets.pokemon.com/assets/cms/pdf/tcg/carddex/bw_dragons_exalted.pdf

* Pokémon TCG Online uses ``{G|R|W|L|P|F|D|M|N|Y|C}}`` in its deck
  export functionality, as well as allowing users to type those
  symbols in chat.

  [Source?]

* Pokémon TCG Card Dex uses these codes in its internal data.

  [Source: trust me bro?]

* Pokémon TCG Live uses these codes in its deck export functionality,
  as well as in its internal data.

  [Source? JP said so. Better source?]


Pokémon ex
^^^^^^^^^^

The "ex" symbol, representing both Pokémon ex and Tera Pokémon ex, is
represented simply by the string "ex". The current version of this
specification does not provide a discrete method in markup for
differentiating between the blue and silver "ex" symbol used for
Pokémon ex or the crystalline "ex" used for Tera Pokémon ex.

How to write "ex" card names in plain text for each language can be
determined by consulting the Pokémon ex Rule Box. Languages that use
the phrase "Pokémon ex" in the Rule Box use a space between the
Pokémon name and "ex". Languages that use the phrase "Pokémon-ex" use
a hyphen between the Pokémon name and "ex".

.. card_figure:: name_sel_ex.png
   :exp: svbsp
   :lang: en
   :num: 004
   :region: pokemon_name_box
   :alt: Mimikyu ex

   The name of this card is ``"Mimikyu ex"``

.. card_figure:: name_sel_tera-ex.png
   :exp: sv1
   :lang: en
   :num: 032
   :region: pokemon_name_box
   :alt: Arcanine ex

   The name of this card, even though it uses the Tera "ex" symbol, is
   ``"Arcanine ex"``

.. card_figure:: name_sel_ex_de.png
   :exp: sv3-5
   :lang: de
   :num: 199
   :region: pokemon_name_box
   :alt: Glurak-ex

   The name of this card is ``"Glurak-ex"``, following localization
   rules for German.


.. caution::

   Different languages have different conventions for how to
   write the name of a "Pokémon ex" in plaintext.

   A non-exhaustive list of examples:

   :English:  | "Arcanine ex"
	      | https://www.pokemon.com/us/pokemon-tcg/pokemon-cards/series/sv01/32/
   :Español:  | "Arcanine ex"
	      | https://www.pokemon.com/es/jcc-pokemon/cartas-pokemon/series/sv01/32/
   :Deutsch:  | "Arkani-ex"
	      | https://www.pokemon.com/de/pokemon-sammelkartenspiel/pokemon-karten/series/sv01/32/
   :Italiano: | "Arcanine-ex"
	      | https://www.pokemon.com/it/gcc/archivio-carte/series/sv01/32/
   :Français: | "Arcanin-ex"
	      | https://www.pokemon.com/fr/jcc-pokemon/cartes-pokemon/series/sv01/32/
   :português brasileiro: | "Arcanine ex"
			  | (No authoritative public source known at this time.)
   :日本語: | "ウインディex"
	    | https://www.pokemon-card.com/card-search/details.php/card/42640/regu/all

Gender symbols
^^^^^^^^^^^^^^

Cards with ♀ or ♂ in the title use the UTF-8 characters for
those symbols. The Emoji selector is not used.

.. card_figure:: name_sel_male.png
   :exp: sv3-5
   :lang: en
   :num: 032
   :region: pokemon_name_box
   :alt: Nidoran♂

   The name of this card is ``"Nidoran♂"``.

.. card_figure:: name_sel_female.png
   :exp: sv3-5
   :lang: en
   :num: 029
   :region: pokemon_name_box
   :alt: Nidoran♀

   The name of this card is ``"Nidoran♀"``.

.. note::
   Canonically, there is no space in "Nidoran♀" nor
   "Nidoran♂", in English, French, German, Italian, Spanish or
   Portuguese.

   See also:

   * https://www.pokemon.com/us/pokedex/nidoran-female
   * https://www.pokemon.com/us/pokedex/nidoran-male
   * https://handy.trainingcourt.com/pokedex-names/


Prefixes
^^^^^^^^

Regional form Pokémon have their form names typeset in a smaller
font. The form name is still part of the card name, and is not marked
up in any special way.

.. card_figure:: name_sel_prefix_regional_en.png
   :exp: sv2
   :lang: en
   :num: 028
   :region: pokemon_name_box
   :alt: Paldean Tauros

   The name of this card is ``"Paldean Tauros"``

Some Trainer cards have a prefix that is similarly typeset, but those
prefixes are also part of the card name and are not marked up.

.. card_figure:: name_sel_prefix_trainer_en.png
   :exp: sv3-5
   :lang: en
   :num: 153
   :region: trainer_name_box
   :alt: Antique Helix Fossil

   The name of this card is ``"Antique Helix Fossil"``


``subtitle`` (i18n str, Optional)
---------------------------------

Some cards feature a subtitle that is not considered to be part of the
card's `name`. Subtitles are typeset using a smaller, gray font that
is right-justified. The subtitle field is any remainder of the text in
the title area that is not part of the canonical `name` of the card.

This field may be omitted when there is no subtitle.

Cards in-scope (SV-on) at time of writing that have subtitles:

* Boss's Orders (ボスの指令)
* Professor's Research (博士の研究)

There may be future additions to this list. The normative references
are the `Pokémon Trading Card Game Rulebook
<https://www.pokemon.com/us/pokemon-tcg/rules>`_ and `Official errata
announcements
<https://www.pokemon.com/us/pokemon-news/changes-coming-to-the-pokemon-tcg-with-sword-shield>`_
which define what text is or is not part of a card's name.

.. card_figure:: subtitle_sel_ghetsis_en.png
   :exp: sv2
   :lang: en
   :num: 265
   :region: trainer_subtitle_box
   :alt: Boss's Orders (Ghetsis)

   The subtitle of this card is ``"Ghetsis"``

.. card_figure:: subtitle_sel_sada_de.png
   :exp: sv1
   :lang: de
   :num: 240
   :region: trainer_subtitle_box
   :alt: Forschung des Professors (Prof. Antiqua)

   The subtitle of this card is ``"Prof. Antiqua"``.

.. card_figure:: subtitle_sel_turo_es.png
   :exp: sv1
   :lang: es
   :num: 241
   :region: trainer_subtitle_box
   :alt: Investigación de Professores (Profesor Turo)

   The subtitle of this card is ``"Profesor Turo"``.


.. note::

   By convention, these subtitles may be printed in parentheses
   alongside the name of the card, e.g. "Professor's Research
   (Professor Sada)". The parentheses are not part of the subtitle
   field. Parentheses are not currently known to appear in the name of
   any card.

   For example, refer to the Pokemon Trading Card Game Database for
   how they display `Professor's Research (Professor Oak)
   <https://www.pokemon.com/us/pokemon-tcg/pokemon-cards/series/cel25/24/>`_.


``artists`` (object, Optional)
------------------------------

Object describing illustrator credits; may be omitted for cards that
do not have any visible illustrator credits such as Basic Energy
cards.

``artists.text`` (i18n str, Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The illustrator credit and prefacing credit type, as it appears on the
card. The prefacing credit type is preserved exactly as it appears on
the card. The overwhelming majority of credits are "Illus." (or
localized equivalent), but other forms of credits have shown up on
occasion.

.. card_figure:: artists_en.png
   :exp: sv3-5
   :lang: en
   :num: 001
   :region: pokemon_artist_box
   :bottom: true
   :alt: MEW_EN Bulbasaur 001/165

   ``"Illus. Yuu Nishida"``

.. card_figure:: artists_it.png
   :exp: sv3-5
   :lang: it
   :num: 006
   :region: pokemon_artist_box
   :bottom: true
   :alt: MEW_IT Charizard-ex 006/165

   ``"Ill. PLANETA Mochizuki"``

.. card_figure:: artists_de.png
   :exp: sv3-5
   :lang: fr
   :num: 173
   :region: pokemon_artist_box
   :bottom: true
   :alt: MEW_DE Pikachu 173/165

   ``"Illustr. Hiroyuki Yamamoto"``

.. card_figure:: artists_es.png
   :exp: sv3-5
   :lang: es
   :num: 195
   :region: pokemon_artist_box
   :bottom: true
   :alt: MEW_ES Ayuda de Dalia 195/165

   ``"Ilus. Fumie Kittaka"``

.. card_figure:: artists_ptbr.png
   :exp: sv3-5
   :lang: en
   :num: 203
   :region: pokemon_artist_box
   :bottom: true
   :alt: MEW_PT Convite da Érica 203/165

   ``"Ilust. Cona Nitanda"``

``artists.list`` (i18n str[], Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An ordered array of credited artists that appear on the card, with the
prefacing credit type removed.

The artists should otherwise be written as they are credited on the
card. In the event that an artist is credited differently in different
localizations of the card, that difference should also be reflected in
this field and not be normalized.

.. card_figure:: artists2_en.png
   :exp: sv1
   :lang: en
   :num: 188
   :region: pokemon_artist_box
   :bottom: true
   :alt: SVI_EN Potion 188/198

   ``["Ayaka Yoshida"]``

.. card_figure:: artists2_it.png
   :exp: sv2
   :lang: it
   :num: 277
   :region: pokemon_artist_box
   :bottom: true
   :alt: PAL_IT Recupero di Energia Plus 277/193

   ``["Studio Bora Inc."]``

.. card_figure:: artists2_de.png
   :exp: sv3
   :lang: de
   :num: 225
   :region: pokemon_artist_box
   :bottom: true
   :alt: OBF_DE Tauboss-ex 225/197

   ``["Jerky"]``

.. card_figure:: artists2_es.png
   :exp: sv2
   :lang: es
   :num: 240
   :region: pokemon_artist_box
   :bottom: true
   :alt: PAL_ES Tinkaton ex 240/193

   ``["takuyoa"]``

.. card_figure:: artists2_ptbr.png
   :exp: sv2
   :lang: ptbr
   :num: 097
   :region: pokemon_artist_box
   :bottom: true
   :alt: PAL_PT Mimikyu 097/193

   ``["Kagemaru Himeno"]``


``regulation_mark`` (i18n str, Optional)
----------------------------------------

The regulation mark, as written. Since the mark was introduced, it has
been a single capital letter in the lower-left of the card face. Legal
values for TPCi-published cards are currently "D", "E", "F" and "G".

All SV cards published in TCGL to date are regulation mark "G".

Basic Energy cards do not have a regulation mark, in which case this
field will be omitted, or null.

.. card_figure:: regulation_mark.png
   :exp: svbsp
   :lang: en
   :num: 037
   :region: regulation_mark_box
   :bottom: true
   :alt: SVP_EN Cleffa 037

   ``"regulation_mark": "G"``

.. admonition:: Out of scope

   How to represent the "Ditto" regulation mark from the Sword &
   Shield special Pokémon Go set is currently out of scope.


``set_icon`` (i18n str, Required)
---------------------------------

A textual representation of the expansion icon that appears in the
lower-left hand of the card, to the right of the Regulation Mark.

In Scarlet & Violet, all cards including energy cards and promotional
cards have a text-based set icon.

The language code that appears in a smaller font to the right of the
expansion code should be separated by an underscore. (This decision is
fully arbitrary, but is for readability.)

.. admonition:: Out of scope

   How to represent graphical icons from earlier sets that do not have
   intuitive text representations is out of scope and will be
   addressed in a subsequent revision of the specification.

.. card_figure:: set_icon_SVI_EN.png
   :exp: sv1
   :lang: en
   :num: 068
   :region: set_icon_box
   :bottom: true
   :alt: SVI_EN Pachirisu 068/198

   ``"SVI_EN"``

.. card_figure:: set_icon_PAL_FR.png
   :exp: sv2
   :lang: fr
   :num: 093
   :region: set_icon_box
   :bottom: true
   :alt: PAL_FR Dedenne-ex 093/193

   ``"PAL_FR"``

.. card_figure:: set_icon_OBF_IT.png
   :exp: sv3
   :lang: it
   :num: 202
   :region: set_icon_box
   :bottom: true
   :alt: OBF_IT Cleffa 202/197

   ``"OBF_IT"``

.. card_figure:: set_icon_MEW_DE.png
   :exp: sv3-5
   :lang: de
   :num: 198
   :region: set_icon_box
   :bottom: true
   :alt: MEW_DE Bisaflor-ex 198/165

   ``"MEW_DE"``

.. card_figure:: set_icon_SVP_ES.png
   :exp: svbsp
   :lang: es
   :num: 027
   :region: set_icon_box
   :bottom: true
   :alt: SVP_ES Pikachu 027

   ``"SVP_ES"``

.. card_figure:: set_icon_SVE_PT.png
   :exp: sve
   :lang: ptbr
   :num: 006
   :region: set_icon_box
   :bottom: true
   :alt: SVE_PT Energia {F} Básica

   ``"SVE_PT"``


``collector_number`` (object, Optional)
---------------------------------------

Object describing the "collector number", which appears in the
lower-left hand corner of the card, to the right of the set icon and
to the left of the rarity symbol.

As of Scarlet & Violet, this field is always set; prior expansions do
not have a collector number for setless Basic Energy.


``collector_number.full`` (i18n str, Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is the full text of the collector's number, including the total
number of non-secret cards in the set, if present.

.. card_figure:: collector_number_normal.png
   :alt: MEW_EN Pikachu 025/165
   :exp: sv3-5
   :lang: en
   :num: 025
   :bottom: true
   :region: collector_number_long

   ``"full": "025/165"``

.. card_figure:: collector_number_promo.png
   :alt: SWSHP Pikachu V SWSH285
   :exp: swshbsp
   :lang: en
   :num: 285
   :bottom: true
   :region: collector_number_longer

   ``"full": "SWSH285"``

.. card_figure:: collector_number_tg.png
   :alt: LOR Pikachu TG05/TG30
   :exp: swsh12a
   :lang: en
   :num: 005
   :bottom: true
   :region: collector_number_longer

   ``"full": "TG05/TG30"``


``collector_number.numerator`` (i18n str, Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The first portion of the collector's number, excluding the number of
non-secret cards in the set.

.. card_figure:: collector_number_normal.png
   :alt: MEW_EN Pikachu 025/165
   :exp: sv3-5
   :lang: en
   :num: 025
   :bottom: true
   :region: collector_number_long

   ``"numerator": "025"``

.. card_figure:: collector_number_promo.png
   :alt: SWSHP Pikachu V SWSH285
   :exp: swshbsp
   :lang: en
   :num: 285
   :bottom: true
   :region: collector_number_longer

   ``"numerator": "SWSH285"``

.. card_figure:: collector_number_tg.png
   :alt: LOR Pikachu TG05/TG30
   :exp: swsh12a
   :lang: en
   :num: 005
   :bottom: true
   :region: collector_number_longer

   ``"numerator": "TG05"``

.. note::

   In practice, this is the same as the "full" field, but omitting
   everything from the first "/" onwards. The intent is to be
   "separator agnostic" here; in the event that other separators are
   ever used, this field should still represent only information as it
   pertains to this specific card. (i.e. the number of THIS card,
   without set information.)

.. admonition:: RFC

   Is there a better name than "numerator"? As Egon points out, it
   isn't exactly a fraction. Egon uses "left part" and "right part"
   which I just find a little too generic for my tastes.

``collector_number.denominator`` (i18n str, Optional)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The second portion of the collector's number, the number of non-secret
cards in the set.

.. card_figure:: collector_number_normal.png
   :alt: MEW_EN Pikachu 025/165
   :exp: sv3-5
   :lang: en
   :num: 025
   :bottom: true
   :region: collector_number_long

   ``"denominator": "165"``

.. card_figure:: collector_number_promo.png
   :alt: SWSHP Pikachu V SWSH285
   :exp: swshbsp
   :lang: en
   :num: 285
   :bottom: true
   :region: collector_number_longer

   ``"denominator": null``

.. card_figure:: collector_number_tg.png
   :alt: LOR Pikachu TG05/TG30
   :exp: swsh12a
   :lang: en
   :num: 005
   :bottom: true
   :region: collector_number_longer

   ``"denominator": "TG30"``

.. note::
   In practice, this is the same as the "full" field,
   but omitting every character until one after the first "/" is
   encountered.

``collector_number.numeric`` (int, Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A numeric version of the "numerator" field, which necessitates the
removal of any non-numeric information that may appear in the
collector number.

e.g. the full collector number "TG05/TG30" would have a numeric value
of `5`.

.. note::
   Although it's out of scope, The intent for unown cards that feature
   collector's numbers written as "ONE", "TWO", "THREE", and "FOUR" in
   unown script would produce the numeric values 1, 2, 3 and 4 here.

``rarity`` (object, Optional)
-----------------------------

Object representing rarity information. Some cards do not have any
rarity information present, such as basic energy cards.

``rarity.designation`` (enum str, Requred)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The rarity designation of the card.

Since Scarlet & Violet, this designation correlates 1:1 with the
rarity symbol printed on the card, but this has not always been
historically true. Thus, while the official rarity of the card can be
determined in recent cases by the symbol on the card, other cases must
be determined by consulting the Player's Guidebook included in the
Elite Trainer Box.

The Player's Guidebook is considered the authoritative source because
it covers secret cards, unlike the online checklist PDFs provided by
TPCi which do not.

Must be one of:

* ``PROMO`` — For all cards featuring the |BSP| icon
* ``COMMON``
* ``UNCOMMON``
* ``RARE``
* ``DOUBLE_RARE``
* ``ILLUSTRATION_RARE``
* ``ULTRA_RARE``
* ``SPECIAL_ILLUSTRATION_RARE``
* ``HYPER_RARE``

.. note::
   For Scarlet & Violet, the rarity designation correlates directly and
   unambiguously with the rarity icon. That mapping is:

   ============================= ======
   Designation                   Symbol
   ============================= ======
   ``PROMO``                     |BSP|
   ``COMMON``                    |CO|
   ``UNCOMMON``                  |UN|
   ``RARE``                      |RA|
   ``DOUBLE_RARE``               |DR|
   ``ILLUSTRATION_RARE``         |IR|
   ``ULTRA_RARE``                |UR|
   ``SPECIAL_ILLUSTRATION_RARE`` |SIR|
   ``HYPER_RARE``                |HR|
   ============================= ======

   In the event that this property won't hold for future sets, the ETB
   booklet is to be treated as the authoritative source of the rarity
   designation for each card.

.. admonition:: Out of scope

   The rarity designation for cards released prior to the introduction
   of ETB booklets is out of scope for this document and will be
   addressed later.

   In general, the canonical sources of rarity information are:

   1) Wizards of the Coast oracle text
   2) Physical checklists packaged with theme deck products (Through end of WOTC)
   3) Pokémon USA Card Dex PDFs (through mid-BW)

   In some cases, these sources disagree particularly on "secret"
   cards, which will require closer scrutiny and review at a later
   date.

``rarity.icon`` (enum str, Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A description of the rarity icon symbol itself as it appears on the
card.

One of:

* ``BLACK_STAR_PROMO`` — |BSP|
* ``BLACK_CIRCLE`` — |CO|
* ``BLACK_DIAMOND`` — |UN|
* ``BLACK_STAR`` — |RA|
* ``TWO_BLACK_STARS`` — |DR|
* ``GOLD_STAR`` — |IR|
* ``TWO_SILVER_STARS`` — |UR|
* ``TWO_GOLD_STARS`` — |SIR|
* ``THREE_GOLD_STARS`` — |HR|

There are no known official sources for names of these icons, so they
have been arbitrarily chosen here. If new rarity icons are introduced,
we will unfortunately require an amendment to this specification.


``copyright`` (object, Required)
--------------------------------

Object representing copyright information at the center bottom of the
card.

``copyright.text`` (i18n str, Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Copyright text as it appears on the card, exactly as written.

Example: The Bulbasaur 001/165 card from the Scarlet & Violet-151
expansion features the copyright text ``"©2023 Pokémon / Nintendo /
Creatures / GAME FREAK"``

``copyright.year`` (int, Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The most recent year in the copyright string as an
integer. e.g. ``"©2023 Pokémon / Nintendo / Creatures / GAME FREAK"``
would be represented as ``2023``.

``tags`` (enum str[], Optional)
-------------------------------

An unordered array of enumeration strings that describe miscellaneous
attributes of the card.

There are two categories of tags; "markers" and "attributes". They are
not distinguished in data, but are separated here for clarity.

Markers
^^^^^^^

Marker-style tags represent a graphical design element on the
card. The enumerations used to represent them are simply upper-cased
representations of their English text.

``ANCIENT``
~~~~~~~~~~~

This is a Paradox Pokémon, Ancient type. The card says "ANCIENT" (or
the localized equivalent) on the right-hand side of the art window.

``FUTURE``
~~~~~~~~~~

This is a Paradox Pokémon, Future type. The card says "FUTURE" (or the
localized equivalent) on the right-hand side of the art
window.


Attributes
^^^^^^^^^^

Attribute-style tags are potentially redundant information about a
card that may or may not be represented by an explicit graphical
element on the card.

These tags bend the rules for "As written" in favor of convenience.

``EX_LOWER``
~~~~~~~~~~~~

This is a Pokémon ex. While it is sufficient to say that a Pokémon is
"ex" if its name ends with "ex" and it has an "ex rule" on the card,
localization may make this determination tricky.

As a convenience, the ``EX_LOWER`` tag is used to unambiguously
specify that this is a Pokémon ex.

``TERA``
~~~~~~~~

This is a Tera type Pokémon ex. While it is sufficient to say that a
Pokémon is a Tera type if it has the Tera effect, localization may
make this determination difficult and this tag is provided as a
convenience.

``ITEM``
~~~~~~~~

This is an Item card. Due to the frequently changing categorization of
Item cards over the years, this tag is provided as a
meta-classification convenience. It is set on:

  * Any ``TRAINER`` card with no subtype through the first three
    generations of the game. These cards are understood by modern
    rulesets to be the spiritual equivalent of what "Item" cards are
    now.

  * Any "Trainer - Trainer" type card from the fourth generation
    of cards.

  * Any ``TRAINER`` - ``ITEM`` type card from the fifth generation of cards
    onward.


``TOOL``
~~~~~~~~

This is a Pokémon Tool card. Due to the frequently changing
categorization of Pokémon Tool cards over the years, this tag is
provided as a meta-classification convenience. It is set on:

* ``TRAINER`` cards that specify "Pokémon Tool" as a subtype in the
  2nd through 4th generation of cards.

* ``TRAINER`` - ``ITEM`` cards that feature the "Pokémon Tool" rule
  text in the 5th through 8th generation of cards. (These cards will
  also be tagged as ``ITEM``.)

* ``TRAINER`` - "Pokémon Tool" cards from the 9th generation of cards
  (and presumably subsequent generations).

``PLAYABLE_TRAINER``
~~~~~~~~~~~~~~~~~~~~

This is a ``TRAINER`` card that can be played as a Pokémon under
certain conditions. It has HP and may have Attacks/Abilities unlike
normal Trainer cards.


``size`` (enum str, Required)
-----------------------------

An enumeration that describes the physical size of the card.

This is always ``STANDARD`` for cards in TCGL, whose real world
equivalents are 63mm x 88mm. Other enumerations are not currently
defined.


``back`` (enum str, Required)
-----------------------------

An enumeration that describes the design on the card back.

This is one of:

* ``POCKET_MONSTERS_1996``
* ``POKEMON_1999``
* ``POKEMON_2001``

This is always ``POKEMON_1999`` for cards in TCGL, which is the classic
design introduced in 1999 and has been the exclusive card back for all
competition legal cards produced in NA, LATAM and Europe.

Although unused in the current scope of this specification,
``POKEMON_2001`` represents the redesigned Japanese back while
``POCKET_MONSTERS_1996`` represents the classic Japanese design.

This field requires some coordination from a standards authority
contrary to our stated goal; however, the design of the back is
unlikely to change very often and it should not pose a problem in
practice.

``foil`` (object, Optional)
---------------------------

Object representing the foil attributes of the card, if any. If the
card does not have any foiling or etching, this field can be omitted.

``foil.type`` (enum str, Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An enumeration representing the type of foil layer present on the physical card.

This is one of:

* ``SV_HOLO``
* ``SV_ULTRA``
* ``FLAT_SILVER``
* ``SUN_PILLAR``
* ``COSMOS``

.. admonition:: FIXME

   How to identify the foil layers is a work in progress. We know
   which foil layers are used for which cards from internal data in
   PTCGO, PTCGL and Card Dex. However, not all cards are present in
   these sources and we'll need to define the foil layers in terms of
   observable properties in order to add promotional variants in the
   future.

   We'll need good pictures that illustrate the foil layers. I've
   stripped some cards clean with acetone and am in the process of
   getting convincing, representative photos of each layer.

   * ``SV_HOLO`` is the standard foil layer used for Rare cards in the
     SV era. It looks like mostly horizontal prismatic bands.

   * ``FLAT_SILVER`` is the standard foil layer used for most parallel
     holo cards - though the parallel holo mask design may change from
     generation to generation or card type to card type, the foil
     layer itself is a simple flat reflective layer.

   * ``SUN_PILLAR`` is used for |DR|, |IR| and |UR| cards. It has a
     characteristic diagonal prismatic banding that is easy to see
     even on cards with etching.

   * ``SV_ULTRA`` also features horizontal prismatic banding, but it
     has fewer bands visible at a time than ``SV_HOLO`` does. The
     defining characteristic is the addition of a prismatic "glitter"
     effect not present in the ``SV_HOLO`` layer. It may be tough to
     identify this foil through the etching layer, but the presence of
     a prismatic glitter effect on the card border should be easier to
     identify. It is used for Tera Pokémon ex |DR|, Tera Pokémon ex
     |UR|, |SIR| and |HR| cards.

   * ``COSMOS`` is the well-known holographic foil layer used mainly
     for promotional cards.

.. admonition:: Out of Scope

   The difference between "pixel" cosmos and "smooth" cosmos is out of
   scope for now, but will be addressed in a future revision.


The names of these foil types have been taken from the internals of
Pokémon: Trading Card Game Online, Pokémon: Trading Card Game Live,
and Pokémon Card Dex applications, and are not expected to
change. However, some foil types are not covered by these sources and
will require additional review and scrutiny when adding new types in
the future.


``foil.mask`` (enum str, Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An enumeration that describes where the foil is visible on the card.

One of:

* ``HOLO`` — For cards where the holographic effect is applied to the
  illustration on the card, both for full arts and typical cards.

* ``REVERSE`` — When the holographic effect is applied everywhere
  *except* the illustration on the card.

* ``ETCHED`` — When the card has a texture created by etching grooves
  into a foil layer. This is used for Tera Pokémon ex |DR|, |UR|,
  |SIR| and |HR| cards.

* ``STAMPED`` — When the only visible foil is a stamped logo. Most
  prominently, this refers to the WORLDS promotional cards.

.. note::

   The names here are chosen based on internal data from PTCGO, PTCGL,
   and Card Dex. These distinctions have been present for long enough
   and over a wide enough period of time that they are unlikely to
   change, so we are seeking to avoid re-defining our own
   classifications from the ground-up.

.. admonition:: RFC

   This classification might leave some things to be desired, though:
   note that |DR| cards do not have any meaningful description that
   indicates there are little stars present. We also do not have a
   field at present that describes or categorizes the foil patterns
   visible for the reverse holos, etc.

   More or less, though, the thought is: "If it's good enough for
   TPCi, it's probably good enough for us."

   I'm not against adding additional foil fields to describe other
   properties of the foil if we decide we really need that in the
   future; for example to differentiate "Holo on just the art window"
   vs "Holo over the entire card" which is otherwise uncaptured by the
   "mask" field as-is.

``text`` (object[], Optional)
-----------------------------

An ordered array of objects representing gameplay-relevant text
elements on the card, in order of their appearance on the card. For
all locales in-scope, that order is top-to-bottom and left-to-right.

It may be omitted for cases where there is no text, such as for Basic
Energy cards.

Fields common to all (or, most) text elements are "kind", "name" and
"text"; though not all entries will set "name". The "kind" field can
be considered as a tagged union discriminator; the following sections
describe the text elements for each enumeration of "kind".

The "name" and "text" fields, where present, represent what is written
on the card verbatim; they are not enumerations and their values will
vary with the language of the card.

``ABILITY``
^^^^^^^^^^^

``ABILITY`` text represents a literal Pokémon Ability, printed
strictly as "Ability" (or the localized equivalent). It
does not include "Pokémon Power", "PokePower", "PokeBody", etc.

It is represented as follows::

  {
    "kind": "ABILITY",
    "name": "Counterattack Quills",
    "text": "If this Pokémon is in the Active Spot and is damaged by an attack from your opponent's Pokémon (even if this Pokémon is Knocked Out), put 3 damage counters on the Attacking Pokémon."
  }


``ATTACK``
^^^^^^^^^^

``ATTACK`` text represents an Attack, which always has a cost symbol, but may
sometimes omit damage or text.

A basic attack is represented as follows::

  {
    "kind": "ATTACK",
    "name": "Superpowered Throw",
    "text": "This attack does 30 more damage for each {C} in your opponent's Active Pokémon's Retreat Cost.",
    "cost": [ "GRASS", "COLORLESS" ],
    "damage": {
      "amount": 10,
      "suffix": "+"
    }
  }

The ``name`` field should always be defined.

The ``text`` field may be omitted for Attacks that don't have any
descriptive text. (Attacks that describe only a cost and damage
are common.)

The ``cost`` field is an ordered array of types, with the order
matching the order of the symbols comprising the Attack cost on the
card. Attacks that do not require energy (This can be seen on Basic
regional forms fairly often) use an explicit cost of ``["FREE"]``
instead.

The ``damage`` field is an Optional compound object that can be
omitted for attacks that do not describe a damage (and only describe
some effect via the ``text``). ``damage.amount`` is a Required
subfield that describes the integer amount of the damage as
written. ``damage.suffix`` is an Optional subfield that can be the
string ``"×"``, ``"+"``, or ``"-"`` when necessary.


``EFFECT``
^^^^^^^^^^

``EFFECT`` text represents some "effect" a Pokémon card has that is
not considered to be an ``ATTACK`` nor an ``ABILITY``.

The name was chosen from the EN rulebook, which states: "Tera Pokémon
ex have a crystalline appearance and a new effect that prevents all
attack damage done to them while they’re on your Bench. This effect
applies to all attacks, both yours and your opponent’s."

It is represented as follows::

  {
      "kind": "EFFECT",
      "name": "Tera",
      "text": "As long as this Pokémon is on your Bench, prevent all damage done to this Pokémon by attacks (both yours and your opponent’s)."
  }


``REMINDER``
^^^^^^^^^^^^

``REMINDER`` text represents reminder text that appears on cards
calling attention to rules from the rulebook, but don't introduce new
gameplay attributes in and of themselves.

Reminder text is represented as::

  {
    "kind": "REMINDER",
    "text": "You may play only 1 Supporter card during your turn."
  }


``RULE_BOX``
^^^^^^^^^^^^

``RULE_BOX`` text represents "Rule Boxes" on cards, such as the
"Pokémon ex rule". It is only used for rules which are considered to
be a "Rule Box" for the purposes of cards such as `Artazon
<https://pkmncards.com/card/artazon-paldea-evolved-pal-171/>`_.

Rule boxes are represented as::

  {
    "kind": "RULE_BOX",
    "name": "Pokémon ex rule",
    "text": "When your Pokémon ex is Knocked Out, your opponent takes 2 Prize cards."
  }


``TEXT_BOX``
^^^^^^^^^^^^

``TEXT_BOX`` text represents unstructured card text for ``TRAINER``
and ``ENERGY`` cards.

Text boxes are represented as::

  {
    "kind": "TEXT_BOX",
    "text": "Search your deck for an Item card and a Pokémon Tool card, reveal them, and put them into your hand. Then, shuffle your deck."
  }


Pokémon card fields
===================

``stage`` (enum str, Required)
------------------------------

An enumeration describing a Pokémon's stage. The enumeration is based
on the English text present in the upper-left corner of the Pokémon
card.

One of:

* ``BASIC``
* ``STAGE1``
* ``STAGE2``

See `Appendix A` for localizations.

``BASIC``
^^^^^^^^^

A Pokémon's stage is ``BASIC`` if "BASIC" (or the localized
equivalent) is written in the top-left corner of the card.

.. card_figure:: pokemon_stage_basic_en.png
   :exp: sv1
   :lang: en
   :num: 073
   :region: pokemon_stage_box

.. card_figure:: pokemon_stage_basic_de.png
   :exp: sv1
   :lang: de
   :num: 073
   :region: pokemon_stage_box

.. card_figure:: pokemon_stage_basic_es.png
   :exp: sv1
   :lang: es
   :num: 073
   :region: pokemon_stage_box

.. card_figure:: pokemon_stage_basic_fr.png
   :exp: sv1
   :lang: fr
   :num: 073
   :region: pokemon_stage_box

``STAGE1``
^^^^^^^^^^

A Pokémon's stage is ``STAGE1`` if "STAGE 1" (or the localized
equivalent) is written in the top-left corner of the card.

.. card_figure:: pokemon_stage_stage1_en.png
   :exp: sv1
   :lang: en
   :num: 075
   :region: pokemon_stage_box

.. card_figure:: pokemon_stage_stage1_fr.png
   :exp: sv1
   :lang: fr
   :num: 075
   :region: pokemon_stage_box

.. card_figure:: pokemon_stage_stage1_it.png
   :exp: sv1
   :lang: it
   :num: 075
   :region: pokemon_stage_box

.. card_figure:: pokemon_stage_stage1_de.png
   :exp: sv1
   :lang: de
   :num: 075
   :region: pokemon_stage_box

.. card_figure:: pokemon_stage_stage1_ptbr.png
   :exp: sv1
   :lang: ptbr
   :num: 075
   :region: pokemon_stage_box

``STAGE2``
^^^^^^^^^^

A Pokémon's stage is ``STAGE2`` if "STAGE 2" (or the localized
equivalent) is written in the top-left corner of the card.

.. card_figure:: pokemon_stage_stage2_en.png
   :exp: sv3
   :lang: en
   :num: 073
   :region: pokemon_stage_box

.. card_figure:: pokemon_stage_stage2_fr.png
   :exp: sv3
   :lang: fr
   :num: 073
   :region: pokemon_stage_box

.. card_figure:: pokemon_stage_stage2_es.png
   :exp: sv3
   :lang: es
   :num: 073
   :region: pokemon_stage_box

.. card_figure:: pokemon_stage_stage2_de.png
   :exp: sv3
   :lang: de
   :num: 073
   :region: pokemon_stage_box

.. card_figure:: pokemon_stage_stage2_ptbr.png
   :exp: sv3
   :lang: ptbr
   :num: 073
   :region: pokemon_stage_box

``stage_text`` (i18n str, Required)
-----------------------------------

Any text, verbatim, as included in the "Stage" graphic element, or
explicit "Evovles into ..." text on Antique fossil cards.

Examples:
 * "Evolves from Gloom"
 * "Entwickelt sich aus Knilz"
 * "Evolves into Omanyte"
 * "Évolue en : Ptéra"

.. card_figure:: evolution_text.png
   :exp: sv1
   :lang: en
   :num: 075
   :region: pokemon_evolution_box
   :alt: SVI Pawmo 075/198

   ``"stage_text": "Evolves from Pawmi"``

.. admonition:: RFC

   Should I rename this one "evolution_text"? It might serve the
   fossil cards from 151 better.

``hp`` (int, Required)
----------------------

The Pokémon's HP, as an integer. The HP is located in the upper-right
corner of the card, to the left of the Pokemon's `types`.

Some ``TRAINER`` cards that can be played as Pokémon also have HP, such as
some fossils and dolls. See the `PLAYABLE_TRAINER` tag.

.. admonition:: Out of scope

    The possibility of non-numerical text may need to be handled;
    for cards like Ancient Mew. Later.

.. card_figure:: pokemon_hp_en.png
   :exp: sv2
   :lang: en
   :num: 062
   :region: pokemon_hp_box
   :alt: Pikachu

   Pikachu's ``hp`` is ``70``.

.. card_figure:: pokemon_hp_de.png
   :exp: sv1
   :lang: de
   :num: 209
   :region: pokemon_hp_box
   :alt: Pamomamo

   Pamomamo's ``hp`` is ``130``.

.. card_figure:: pokemon_hp_es.png
   :exp: sv3-5
   :lang: es
   :num: 205
   :region: pokemon_hp_box
   :alt: Mew ex

   Mew ex's ``hp`` is ``180``.

.. card_figure:: pokemon_hp_fr.png
   :exp: sv3
   :lang: fr
   :num: 082
   :region: pokemon_hp_box
   :alt: Mélodelfe-ex

   Mélodelfe-ex's ``hp`` is ``260``.

.. card_figure:: trainer_hp_en.png
   :exp: sv3-5
   :lang: en
   :num: 153
   :region: trainer_hp_box
   :alt: Antique Helix Fossil

   Antique Helix Fossil's ``hp`` is ``60``.


``types`` (enum str[], Required)
--------------------------------

The Pokémon's type(s), as represented in the upper-right hand corner
of the card. For compatibility with older expansions that occasionally
feature multi-type Pokémon, this field is an Array instead of a
string.

Each type is one of:

* |G| — ``GRASS``
* |R| — ``FIRE``
* |W| — ``WATER``
* |L| — ``LIGHTNING``
* |P| — ``PSYCHIC``
* |F| — ``FIGHTING``
* |D| — ``DARKNESS``
* |M| — ``METAL``
* |Y| — ``FAIRY``
* |N| — ``DRAGON``
* |C| — ``COLORLESS``

.. card_figure:: pokemon_type.png
   :exp: sv3-5
   :lang: en
   :num: 008
   :region: pokemon_type_box
   :alt: Wartortle

   Wartortle's ``types`` field is ``[ "WATER" ]``.

This field is also defined for ``ENERGY`` - ``BASIC`` cards:

.. card_figure:: energy_type.png
   :exp: sve
   :lang: en
   :num: 007
   :region: energy_type_box
   :alt: Basic {D} Energy

   Basic |D| Energy's ``types`` field is ``[ "DARKNESS" ]``.

``weakness`` (object, Optional)
-------------------------------

The Pokémon's weakness(es), if any. It can be omitted if there are no
weaknesses listed on the card.

``weakness.types`` (enum str[], Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An ordered list of types this Pokémon is weak to; please refer to the
`types` field. If this Pokémon is weak to multiple types, they should
be listed in the same order they are on the card top to bottom, left
to right.

``weakness.amount`` (int, Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The numerical weakness amount as listed on the card. For cards in
scope, this is always ``2``.

``weakness.operator`` (enum str, Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The weakness operator as listed on the card. For cards in-scope, this
is always ``"×"``.

``resistance`` (object, Optional)
---------------------------------

The Pokémon's resistance(s), if any. It can be omitted if there are no
resistances listed on the card.

``resistance.types`` (enum str[], Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An ordered list of types this Pokémon is resistant to; please refer to the
`types` field. If this Pokémon is resistant to multiple types, they should
be listed in the same order they are on the card top to bottom, left
to right.

``resistance.amount`` (int, Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The numerical resistance amount as listed on the card. For cards
in^scope, this is always ``30``.

``resistance.operator`` (enum str, Required)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The resistance operator as listed on the card. For cards in-scope, this is always ``"-"``.


``retreat`` (int, Required)
---------------------------

The retreat cost for this Pokémon, represented as an integer. If there
is no retreat cost printed, this is 0.

``flavor_text`` (i18n str, Optional)
------------------------------------

The flavor text for this Pokémon, written exactly as it appears on the
card if it is present. Flavor text is present on any Pokémon card that
does not feature a Pokémon ex. (This is |CO|, |UN|, |RA| and |IR|
cards, and possibly |HR| on occasion.)


Other fields
============

images
------

.. admonition:: FIXME

		TODO

ext
---

.. admonition:: FIXME

		TODO

Appendix A
==========

.. admonition:: FIXME

		TODO. There will be a localization table here.
