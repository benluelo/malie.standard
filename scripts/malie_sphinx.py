import os
import subprocess
from pathlib import Path

from docutils import nodes
from docutils.parsers.rst import Directive
import docutils.parsers.rst.directives.images
from docutils.parsers.rst.directives.images import Figure

import requests

REGIONS = {
    # common to Trainers, Energy
    'subtype_box':           ( 34, 20, 308,  72),
    'card_type_box':         (360, 20, 720,  72),
    'trainer_name_box':      ( 36, 77, 494, 138),

    # Trainers
    'trainer_subtitle_box':  (519, 89, 699, 125),
    'trainer_hp_box':        (575, 79, 701, 137),

    # Pokemon
    'pokemon_name_box':      (133, 37, 486, 89),
    'pokemon_header_boxes':  [(20, 27, 123, 63), (540, 34, 698, 90)],
    'pokemon_hp_box':        (515, 34, 641, 92),
    'pokemon_stage_box':     ( 20, 27, 123, 63),
    'pokemon_type_box':      (637, 28, 701, 93),
    'pokemon_evolution_box': (135, 91, 350, 120),

    # Energy only
    'energy_type_box':       (637, 72, 701, 137),

    # Regions for ... hopefully many cards?
    'pokemon_artist_box':    (34, 935, 240, 959),
    'regulation_mark_box':   (38, 955, 68, 990),
    'set_icon_box':          (64, 957, 118, 991),
    'collector_number_long': (115, 967, 205, 994),
    'collector_number_longer': (105, 967, 225, 994),
}


def img(exp, lang, num, variant="std"):
    # Plan A: "We just so happen to have the image saved locally!"
    local_root = os.environ.get('MALIE_SPHINX_IMAGE_DIR', '/mnt/pkmndb/tcgl/cards/png')
    path = Path(local_root).joinpath(f"{lang}/{exp}/{exp}_{lang}_{num}_{variant}.png")
    if path.exists():
        return str(path)

    # Alright, plan B.
    filename = f"{exp}_{lang}_{num}_{variant}.png"
    cache_name = Path(f"_build/tmp/{filename}")
    if cache_name.exists():
        return str(cache_name)

    # Don't have it saved already, so go and get it
    url = f"https://cdn.malie.io/file/malie-io/tcgl/cards/png/{lang}/{exp}/{filename}"
    print(f"fetch {url}")
    rsp = requests.get(url, timeout=30)
    rsp.raise_for_status()
    cache_name.parent.mkdir(parents=True, exist_ok=True)
    with open(cache_name, "wb") as outfile:
        outfile.write(rsp.content)
    return str(cache_name)


def draw_boxes(infile, outfile, *args, flip=False):
    if flip:
        crop = f"733x250+0+774"
        mask = "_build/tmp/upmask.png"
    else:
        crop = f"733x250+0+0"
        mask = "_build/tmp/mask.png"

    cli_args = [
        "convert",
        infile,
        "-fill", "none",
    ]

    for coords in args:
        cli_args.extend([
            "-stroke", "white",
            "-strokewidth", "7",
            "-draw", f"roundrectangle {coords[0]},{coords[1]} {coords[2]},{coords[3]} 15,15",
            "-stroke", "black",
            "-strokewidth", "4",
            "-draw", f"stroke-dasharray 5 3 roundrectangle {coords[0]},{coords[1]} {coords[2]},{coords[3]} 15,15",
        ])

    cli_args.extend([
        "-crop", crop,
        "(",
        "-clone", "0",
        "-alpha", "extract",
        "-compose", "Multiply",
        mask,
        "-composite",
        "-alpha", "off",
        ")",
        "-compose", "copyalpha",
        "-composite",
        outfile
    ])

    print(cli_args)
    subprocess.run(cli_args, check=True)
    subprocess.run(["oxipng", "-o", "max", "-a", outfile])


def draw_mask(invert: bool = False):
    args = [
        'convert',
        '-size', '733x250',
        'xc:white',
        '-fill', 'none',
        '-stroke', 'black',
        '-strokewidth', '4',
        '-draw', 'bezier 0,200 244,125 489,275 732,200',
        '-fuzz', '99%',
        '-fill', 'black',
        '-floodfill', '+0+249', 'white',
        '-alpha', 'off',
        '-negate',
        '-motion-blur', '0x10-90',
        '-negate',
    ]

    if invert:
        filename = '_build/tmp/upmask.png'
        args.append('-flip')
    else:
        filename = '_build/tmp/mask.png'

    args.append(filename)
    print(args)
    os.makedirs('_build/tmp', exist_ok=True)
    subprocess.run(args, check=True)


def empty_validator(param, option_arg=None):
    #print(f"param: {param}")
    #print(f"option_arg: {option_arg}")
    return param


class CardFigure(Figure):
    has_content = True
    required_arguments = 1
    optional_arguments = 0
    option_spec = Figure.option_spec.copy()
    option_spec.update({
        'exp':    empty_validator,
        'lang':   empty_validator,
        'num':    empty_validator,
        'region': empty_validator,
        'bottom': empty_validator,
    })

    def run(self):
        target_filename = os.path.join("_build", "html", "_images", self.arguments[0])
        source_filename = img(self.options['exp'], self.options['lang'], self.options['num'])
        boxes = REGIONS[self.options['region']]

        os.makedirs('_build/html/_images', exist_ok=True)

        invert = 'bottom' in self.options

        if invert:
            mask_filename = '_build/tmp/upmask.png'
        else:
            mask_filename = '_build/tmp/mask.png'

        if not os.path.exists(mask_filename):
            draw_mask(invert=invert)

        if not os.path.exists(target_filename):
            print(f"'{target_filename}' doesn't exist, attempting to create figure")
            if isinstance(boxes, list):
                draw_boxes(source_filename, target_filename, *boxes, flip=invert)
            else:
                draw_boxes(source_filename, target_filename, boxes, flip=invert)

        #print(f"options (as of run()): {self.options}")
        #print(f"arguments (as of run()): {self.arguments}")

        self.arguments[0] = target_filename
        return super().run()


def setup(app):
    app.add_directive("card_figure", CardFigure)

    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
